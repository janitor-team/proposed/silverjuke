Source: silverjuke
Section: sound
Priority: optional
Maintainer: Dr. Tobias Quathamer <toddy@debian.org>
Build-Depends: debhelper-compat (= 13),
               libgl1-mesa-dev,
               libgstreamer-plugins-base1.0-dev,
               libgstreamer1.0-dev,
               libsqlite3-dev,
               libupnp-dev,
               libwxgtk3.2-dev,
               python3-docutils,
               zlib1g-dev
Standards-Version: 4.6.1
Homepage: https://www.silverjuke.net/
Vcs-Browser: https://salsa.debian.org/debian/silverjuke
Vcs-Git: https://salsa.debian.org/debian/silverjuke.git

Package: silverjuke
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: jukebox, karaoke, and kiosk mode mp3 player
 Silverjuke is an easy to use jukebox solution implemented in wxWidgets.
 Some of the features are:
  * Wide range of supported audio formats (MP1, MP2, MP3, MPC, MP+,
    Ogg-Vorbis, FLAC, Monkey's Audio, WavPack, WAV, AIFF, MOD),
    more formats can be implemented using modules
  * Internal sound processing with 32 bit (for crossfading, autovolume,
    jingles, smart shuffle, VST effect plugings)
  * Kiosk mode, supports touchscreens
  * Track information can be edited (ID3-tags)
  * Support for playlists (M3U, PLS, CUE)
  * Fully skinnable
  * Powerful search
